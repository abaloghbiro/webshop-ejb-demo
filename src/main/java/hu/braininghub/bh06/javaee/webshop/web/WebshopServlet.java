package hu.braininghub.bh06.javaee.webshop.web;

import java.io.IOException;

import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hu.braininghub.bh06.javaee.webshop.model.Product;
import hu.braininghub.bh06.javaee.webshop.service.CartHolder;

@WebServlet(urlPatterns = "/cart")
public class WebshopServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		CartHolder h = (CartHolder) req.getSession().getAttribute("cartHolder");

		if (h != null) {

			Product p = new Product();
			p.setCode(req.getSession().getId());
			h.addProduct(p);
		} else {
			try {

				InitialContext ctx = new InitialContext();

				for (int i = 0; i < 150; i++) {
					ctx.lookup("java:global/webshop/CartHolder");
				}

				CartHolder newCart = (CartHolder) ctx.lookup("java:global/webshop/CartHolder");

				Product p = new Product();
				p.setCode(req.getSession().getId());
				newCart.addProduct(p);

				req.getSession().setAttribute("cartHolder", newCart);
			}

			catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

}
