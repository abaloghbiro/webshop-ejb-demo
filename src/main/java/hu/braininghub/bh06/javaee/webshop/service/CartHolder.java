package hu.braininghub.bh06.javaee.webshop.service;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.PostActivate;
import javax.ejb.PrePassivate;
import javax.ejb.Stateful;

import hu.braininghub.bh06.javaee.webshop.model.Cart;
import hu.braininghub.bh06.javaee.webshop.model.Product;

@Stateful(name = "CartHolder")
public class CartHolder {

	private final Cart CART = new Cart();

	@PostConstruct
	public void init() {
		System.out.println("init executed...");
	}

	@PrePassivate
	public void prePassivate() {
		System.out.println("prePassivate executed...");
	}

	@PostActivate
	public void postActivate() {
		System.out.println("postActivate executed...");
	}

	public void addProduct(Product p, Integer num) {
		CART.addProduct(p, num);
	}

	
	public void addProduct(Product p) {
		CART.addProduct(p);
	}

	public void removeProduct(Product p) {
		CART.removeProduct(p);
	}

	List<Product> getItems() {
		return CART.getItems();
	}

}
