package hu.braininghub.bh06.javaee.webshop.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Cart {

	private final Map<Product, Integer> PRODUCTS = new HashMap<>();

	public void addProduct(Product p, Integer num) {

		if (PRODUCTS.containsKey(p)) {
			Integer currentNum = PRODUCTS.get(p);
			PRODUCTS.put(p, currentNum + num);
		} else {
			PRODUCTS.put(p, num);
		}
	}

	public void addProduct(Product p) {
		addProduct(p, 1);
	}

	public void removeProduct(Product p) {

		Integer currentNum = PRODUCTS.get(p);

		if (currentNum != null) {

			PRODUCTS.put(p, --currentNum);
		}
	}

	public List<Product> getItems() {
		return new ArrayList<>(PRODUCTS.keySet());
	}

}
